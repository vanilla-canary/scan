import React, { Component } from 'react';
import {
  Text, View, TextInput, Image, PermissionsAndroid, TouchableOpacity, TouchableWithoutFeedback, FlatList, NativeModules, DeviceEventEmitter, Keyboard, Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';

class LoginRouter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',  //账号
      password: '',  //密码

    }
  }

  async componentWillMount () {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: '申请摄像头权限',
        message:
          '一个很牛逼的应用想借用你的摄像头，' +
          '然后你就可以拍出酷炫的皂片啦。',
        buttonNeutral: '等会再问我',
        buttonNegative: '不行',
        buttonPositive: '好吧',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('现在你获得摄像头权限了');
    } else {
      console.log('用户并不屌你');
    }
  }

  componentWillUnmount () {
  }




  // /**
  //  * @description 账号和密码同时有值时返回不同颜色的登录
  //  * @memberof AccountLoginRoute
  //  * @returns 登录按钮颜色
  //  */
  loginButtonColor = () => {
    const { account, password } = this.state
    if (account && account.length > 0 && password && password.length > 0) {
      return "#006BF8" //蓝色
    } else {
      return "#C6C6CD" //灰色
    }
  }

  /**
   * @description 清空账号输入框中的数据
   * @memberof AccountLoginRoute
   */
  clearAccountTextInput = () => {
    this.setState({ account: '' })
  }

  /**
   * @description 点击登录按钮
   * @memberof AccountLoginRoute
   */
  loginOnPress = async () => {
    Keyboard.dismiss()
    const { account, password } = this.state
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(value => {
      if (value) {
        Actions.replace('ScanRouter')
      } else {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: '申请摄像头权限',
            message:
              '一个很牛逼的应用想借用你的摄像头，' +
              '然后你就可以拍出酷炫的皂片啦。',
            buttonNeutral: '等会再问我',
            buttonNegative: '不行',
            buttonPositive: '好吧',
          },
        );
      }
    })
  }

  render () {
    const { account, password, showAccountList } = this.state
    return (
      <View style={{ backgroundColor: '#fff', paddingTop: 40, paddingHorizontal: 24, flex: 1 }}>
        <Text style={{ fontSize: 24, color: "#000000" }}>欢迎使用扫码</Text>
        <View style={{ flexDirection: "row", alignItems: 'center', height: 48, marginTop: 36, borderBottomColor: this.refs.accountTextInput && this.refs.accountTextInput.isFocused() ? "#006BF8" : "#F1F2F3", borderBottomWidth: 1 }}>
          <Text style={{ fontSize: 24, color: "#000000" }}>账号</Text>
          <TextInput
            ref="accountTextInput"
            placeholder="请输入账号"
            value={account}
            maxLength={10}
            onChangeText={text => { this.setState({ account: text }) }}
            style={{ flex: 1, marginLeft: 24, fontSize: 18 }}
          />
          {
            account && account.length > 0 ? <TouchableOpacity onPress={this.clearAccountTextInput}>
              {/* <Image source={require("../../images/login/delete.png")} style={{ width: 16, height: 16, marginRight: 19 }} /> */}
            </TouchableOpacity> : null
          }
        </View>
        <View style={{ flexDirection: "row", alignItems: 'center', height: 48, marginTop: 36, borderBottomColor: this.refs.passwordTextInput && this.refs.passwordTextInput.isFocused() ? "#006BF8" : "#F1F2F3", borderBottomWidth: 1 }}>
          <Text style={{ fontSize: 24, color: "#000000" }}>密码</Text>
          <TextInput
            ref="passwordTextInput"
            secureTextEntry={true}
            placeholder="请输入密码"
            value={password}
            onChangeText={text => { this.setState({ password: text }) }}
            style={{ flex: 1, marginLeft: 24, fontSize: 18 }}
          />
        </View>
        <TouchableOpacity onPress={this.loginOnPress} disabled={this.loginButtonColor() !== "#006BF8"}>
          <View style={{ backgroundColor: this.loginButtonColor(), height: 48, borderRadius: 30, justifyContent: 'center', alignItems: 'center', marginTop: 48 }}>
            <Text style={{ fontSize: 20, color: '#fff' }}>登录</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}


export default LoginRouter;