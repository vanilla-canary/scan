import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import QRCodeView from './QRCodeView'
import { Actions } from 'react-native-router-flux';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window')

/**
 * @description 订单支付
 * @author He Kai
 * @date 2019-12-16
 * @class OrderPayRouteScan
 */
class OrderPayRouteScan extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

   componentWillMount() {
  }


  componentWillUnmount() {
  }


  onBack = () => {
  }

  onScanQRCodeSuccess = async (e) => {
    console.log('onScanQRCodeSuccess',e)
    this.qrCode.destroy();
    Actions.replace('ResultRouter',{code: e})
  }
  render() {
    const {  } = this.state;
      return (
        <View style={{ height: screenHeight, width: screenWidth, backgroundColor: 'transparent' }}>
          <QRCodeView
            ref={(qrCode) => { this.qrCode = qrCode }}
            style={{
              position: 'absolute',
              height: screenHeight,
              width: screenWidth,
            }}
            onScanQRCodeSuccess={(e) => this.onScanQRCodeSuccess(e)}
          />
        </View>
      );
    }
}


export default OrderPayRouteScan;
