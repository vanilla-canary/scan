import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Dimensions } from 'react-native';
import QRCodeView from './QRCodeView'
import { Actions } from 'react-native-router-flux';
const { width: screenWidth, height: screenHeight } = Dimensions.get('window')

/**
 * @description 订单支付
 * @author He Kai
 * @date 2019-12-16
 * @class ResultRouter
 */
class ResultRouter extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

   componentWillMount() {
  }


  componentWillUnmount() {
  }

  loginOnPress =() => {
    Actions.replace('ScanRouter')
  }
  render() {
    const {code = '' } = this.props;
      return (
        <View style={{ height: screenHeight, width: screenWidth, alignItems: 'center', justifyContent: 'center', flexWrap: 'nowrap', backgroundColor: 'transparent' }}>
          <Text style={{ fontSize: 24, color: "#000000" }}>扫码结果</Text>
          <Text style={{ fontSize: 24, color: "#000000" }}>{code}</Text>
          <TouchableOpacity onPress={this.loginOnPress} >
          <View style={{ backgroundColor: '#006BF8', height: 48, borderRadius: 30, justifyContent: 'center', alignItems: 'center', marginTop: 48 }}>
            <Text style={{ fontSize: 20, color: '#fff' }}>继续扫码</Text>
          </View>
        </TouchableOpacity>
        </View>
      );
    }
}


export default ResultRouter;
