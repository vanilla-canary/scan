import React, { Component } from 'react';
import { View, requireNativeComponent, UIManager, findNodeHandle } from 'react-native';
const RCT_QRCODE_REF = 'QRCodeView';
class QRCodeView extends Component {
  _onScanQRCodeSuccess = (event) => {
    if (!this.props.onScanQRCodeSuccess) {
      return;
    }
    this.props.onScanQRCodeSuccess(event.nativeEvent.qrCode);
  }
  destroy = () => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.refs[RCT_QRCODE_REF]),
      UIManager.RCTQRCodeView.Commands.destroy,
      null
    );
  }
  restart = () => {
    UIManager.dispatchViewManagerCommand(
      findNodeHandle(this.refs[RCT_QRCODE_REF]),
      UIManager.RCTQRCodeView.Commands.restart,
      null
    );
  }
  render() {
    return <RCTQRCodeView {...this.props} ref={RCT_QRCODE_REF} onScanQRCodeSuccess={this._onScanQRCodeSuccess} />;
  }
}

QRCodeView.name = "QRCodeView";
QRCodeView.propTypes = {
  // onScanQRCodeSuccess: PropTypes.func,
  // style: View.propTypes.style,
  ...View.propTypes,
};

let RCTQRCodeView = requireNativeComponent('RCTQRCodeView', QRCodeView, {
  nativeOnly: { onChange: true }
});
module.exports = QRCodeView;