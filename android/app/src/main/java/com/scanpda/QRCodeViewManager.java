package com.scanpda;

import android.Manifest;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.util.Xml;
import android.widget.VideoView;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zxing.ZXingView;

import static android.content.Context.VIBRATOR_SERVICE;

public class QRCodeViewManager extends SimpleViewManager<QRCodeView> {

    private MainActivity mMainActivity = null;

    private ThemedReactContext mContext;
    /**
     * 当前组件名
     */
    private static final String QRCODE_VIEW_MANAGER_NAME = "RCTQRCodeView";
    /**
     * 销毁指令
     */
    private static final int COMMAND_DESTROY_ID = 1;
    private static final String COMMAND_DESTROY_NAME = "destroy";
    /**
     * 重启指令
     */
    private static final int COMMAND_RESTART_ID = 2;
    private static final String COMMAND_RESTART_NAME = "restart";

    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of(
                COMMAND_DESTROY_NAME,COMMAND_DESTROY_ID,
                COMMAND_RESTART_NAME,COMMAND_RESTART_ID
        );
    }

    /**
     * 接收指令
     */
    @Override
    public void receiveCommand(QRCodeView view, int commandId, @Nullable ReadableArray args) {
        switch (commandId){
            /**
             * 销毁当前扫码
             */
            case COMMAND_DESTROY_ID:
                view.onDestroy();
                break;
            /**
             * 重启扫码
             */
            case COMMAND_RESTART_ID:
//                if (!mMainActivity.cameraPermission()) {
//                    ActivityCompat.requestPermissions(mMainActivity,
//                            new String[]{Manifest.permission.CAMERA}, MainActivity.QR_CODE_CAMERA_REQUEST_CODE);
//                } else {
                    view.startSpot();
//                }
                break;
            default:
                break;
        }
    }

    @Override
    public String getName() {
        return QRCODE_VIEW_MANAGER_NAME;
    }

    /**
     * 此处创建View实例，并返回
     *
     * @param reactContext
     * @return
     */
    @Override
    protected QRCodeView createViewInstance(ThemedReactContext reactContext) {
        this.mContext = reactContext;

        XmlPullParser parser = reactContext.getResources().getLayout(R.layout.activity_scan);
        AttributeSet attributes = Xml.asAttributeSet(parser);
        LogUtils.d("RCTQRCodeView parser", parser, reactContext.getCurrentActivity(), this);
        int type;
        try {
            while ((type = parser.next()) != XmlPullParser.START_TAG &&
                    type != XmlPullParser.END_DOCUMENT) {
                // Empty

            }
            if (type != XmlPullParser.START_TAG) {
                LogUtils.d("the xml file is error!\n");
            }
        }  catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RCTQRCodeView imageView = new RCTQRCodeView(reactContext, attributes );

//        if (!mMainActivity.cameraPermission()) {
//            ActivityCompat.requestPermissions(mMainActivity,
//                    new String[]{Manifest.permission.CAMERA}, MainActivity.QR_CODE_CAMERA_REQUEST_CODE);
//        } else {
            imageView.startCamera();
            imageView.startSpotAndShowRect();
//        }
        return imageView;
    }

    /**
     * 扫码成功事件实例
     */
    @Override
    public Map getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.of(
                "onScanQRCodeSuccess", MapBuilder.of("registrationName", "onScanQRCodeSuccess"));
    }


    //销毁对象时释放一些资源
    @Override
    public void onDropViewInstance(QRCodeView view) {
        super.onDropViewInstance(view);
        ((ThemedReactContext) view.getContext()).removeLifecycleEventListener((RCTQRCodeView) view);
        view.onDestroy();
    }


    /**
     * 加入生命周期
     */
    private static class RCTQRCodeView extends ZXingView implements LifecycleEventListener, QRCodeView.Delegate {

        private ThemedReactContext themedReactContext;
        private QRCodeView mQRCodeView = null;

        public RCTQRCodeView(ThemedReactContext reactContext, AttributeSet attributeSet) {
            super(reactContext, attributeSet);
            reactContext.addLifecycleEventListener(this);
            themedReactContext = reactContext;
            mQRCodeView = (ZXingView) findViewById(R.id.zxingscanview);
            mQRCodeView.setDelegate(this);
        }

        @Override
        public void onHostResume() {
            LogUtils.d("RCTQRCodeView onHostResume");
        }

        @Override
        public void onHostPause() {
            LogUtils.d("RCTQRCodeView onHostPause");
        }
        /**
         * 播放系统提示音
         */
        private void hint() {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone rt = RingtoneManager.getRingtone(themedReactContext, uri);
            rt.play();
        }

        private void vibrate() {
            Vibrator vibrator = (Vibrator) themedReactContext.getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(200);
        }
        @Override
        public void onHostDestroy() {
            LogUtils.d("RCTQRCodeView onHostDestroy");
        }

        private void dispatchEvent(String eventName,WritableMap eventData){
            ReactContext reactContext = (ReactContext) getContext();
            reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
                    getId(),
                    eventName,
                    eventData
            );
        }
        @Override
        public void onScanQRCodeSuccess(String result) {
            LogUtils.d("RCTQRCodeView onScanQRCodeSuccess", result);
            hint();
            vibrate();
            WritableMap event = Arguments.createMap();
            event.putString("qrCode", result);
            dispatchEvent("onScanQRCodeSuccess", event);
            super.stopSpot();
        }

        @Override
        public void onCameraAmbientBrightnessChanged(boolean isDark) {

        }

        @Override
        public void onScanQRCodeOpenCameraError() {
            LogUtils.d("RCTQRCodeView onScanQRCodeOpenCameraError");
            ToastUtils.showShort("打开相机出错");
        }
    }
}

class QrCodeViewPackage implements ReactPackage {

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    public List<Class<? extends JavaScriptModule>> createJSModules() {
        return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Arrays.<ViewManager>asList(new QRCodeViewManager());
    }
}

