import React, { Component } from 'react';
import { NativeModules, Text, BackHandler, View } from 'react-native';
import {Actions, Router, Stack, Scene} from 'react-native-router-flux'
import {
  ScanRouter, LoginRouter, ResultRouter
} from './routes';


/**
 * @description 路由根容器，提供Dva渲染用
 * @author Rui Hao
 * @date 2019-12-02
 * @class AppRouter
 * @extends {Component}
 */
class App extends Component {
  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.backHandler);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
  }

  backHandler = () => {
    if (Actions.state.routes.length <= 1) {
      return true;
    }
    return false;
  }

  render() {
    return (
      <Router>
        <Stack key="root" >
          <Scene initial key="LoginRouter" hideNavBar component={LoginRouter} />
          <Scene key="ScanRouter" hideNavBar component={ScanRouter} />
          <Scene key="ResultRouter" hideNavBar component={ResultRouter} />
        </Stack>
      </Router>
    );
  }
}


export default App;
